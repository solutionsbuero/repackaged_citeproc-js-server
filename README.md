# citeproc-js-server-forked

A fork of Zoteros citeproc-js-server with the only difference of being a bit adapted to build executable binaries by [pkg](https://github.com/zeit/pkg) (and being a little outdated, probably).

This fork is inteded for easy use of citeproc-js-server in combination with our conversion software [Turandot](https://gitlab.com/solutionsbuero/turandot). Check the installation guide for more information.

For any detailed information on citeproc-js-server, please refer to the softwares [original repo on GitHub](https://github.com/zotero/citeproc-js-server).

## Restrictions

- We currently build for Linux (tested on Ubuntu 18.04), Windows 10 and MacOS running on x64 CPUs, since Turandot is aimed at these architectures/OSes.
- The MacOS build is currently untested (for lack of a testing platform)
- The executables are **not** packed with all their assets and only run if they are left in the repos complete folder structure. Any help on that would be appreciated, though.

